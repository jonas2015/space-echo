﻿using UnityEngine;
using System.Collections;

public class AwardMoney : MonoBehaviour
{
    public float amount = 1f;
    public float waveMultiplier = 0f;

	// Use this for initialization
	void Start ()
    {
        if(waveMultiplier != 0)
            amount = amount * Mathf.Pow(waveMultiplier, GameData.currentWave);
	}

    public void Award()
    {
        GameObject go = GameObject.Find("_UIManager");
        TestLevelUI levelScript = (TestLevelUI) go.GetComponent(typeof(TestLevelUI));
        levelScript.money += amount;
    }
}
