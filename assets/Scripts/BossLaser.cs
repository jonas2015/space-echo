﻿using UnityEngine;
using System.Collections;

public class BossLaser : MonoBehaviour
{
	public GameObject chargeParticles = null;
	public GameObject laserBeam = null;
	public float beamTravelLength = 1.0f;
	public float fireDelay = 1.0f;
	public float fireTime = 1.0f;
	public float damageRate = 1.0f;
	public float damage = 1.0f;
	public float dmgIncreaseMultiplier = 0f;
    public bool shootFromParentPos = true;

	private GameObject invokedBeam = null;
	private LineRenderer beamLineRenderer = null;
	private GameObject invokedParticles = null;
	private bool isEnabled = false;
	private BoxCollider2D boxCollider;
	private Vector2 currentColliderSize;
	private Vector3 beamEndPosition;
	
	// Use this for initialization
	void Start ()
	{
		isEnabled = true;
		if(chargeParticles != null && invokedParticles == null)
			invokedParticles = Instantiate(chargeParticles, transform.position, transform.rotation) as GameObject;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(isEnabled && invokedBeam == null && invokedParticles == null)
			invokedParticles = Instantiate(chargeParticles, transform.position, transform.rotation) as GameObject;
		if(invokedParticles != null)
			invokedParticles.transform.position = transform.position;
		if(invokedBeam != null)
		{
			RaycastHit2D rayHit;
			int layerMask = 1 << 8;
			//layerMask = ~layerMask;
			
			beamEndPosition = new Vector3(transform.position.x, transform.position.y - beamTravelLength, transform.position.z);
			rayHit = Physics2D.Raycast(transform.position, -Vector2.up, Mathf.Infinity, layerMask);
			if(rayHit.collider != null)
			{
				//Debug.Log(rayHit.collider.name);
				if(rayHit.transform.tag == "Player")
				{
					beamEndPosition = rayHit.point;
				}
			}
			
            if(shootFromParentPos)
			    beamLineRenderer.SetPosition(0, transform.parent.position);
            else if(!shootFromParentPos)
                beamLineRenderer.SetPosition(0, transform.position);
			beamLineRenderer.SetPosition(1, beamEndPosition);
			
			currentColliderSize = boxCollider.size;
			boxCollider.transform.position = transform.position + (beamEndPosition - transform.position) / 2;
			boxCollider.size = new Vector2(currentColliderSize.x, (transform.position - beamEndPosition).magnitude);
		}
	}
	
	void FireThaLaser()
	{
		RaycastHit2D rayHit;
		int layerMask = 1 << 8;
		//layerMask = ~layerMask;
		
		invokedBeam = Instantiate(laserBeam, transform.position, transform.rotation) as GameObject;
		invokedBeam.name = invokedBeam.name.Replace("(Clone)", "");
		
		beamLineRenderer = invokedBeam.GetComponent<LineRenderer>();
		
		beamEndPosition = new Vector3(transform.position.x, transform.position.y - beamTravelLength, transform.position.z);
		rayHit = Physics2D.Raycast(transform.position, -Vector2.up, Mathf.Infinity, layerMask);
		if(rayHit.collider != null)
		{
			//Debug.Log(rayHit.collider.name);
			if(rayHit.transform.tag == "Player")
			{
				beamEndPosition = rayHit.point;
			}
		}
        if(shootFromParentPos)
            beamLineRenderer.SetPosition(0, transform.parent.position);
        else if(!shootFromParentPos)
            beamLineRenderer.SetPosition(0, transform.position);
		beamLineRenderer.SetPosition(1, beamEndPosition);
		beamLineRenderer.SetVertexCount(2);
		
		boxCollider = invokedBeam.GetComponent<BoxCollider2D>();
		//boxCollider.center = Vector2.zero;
		boxCollider.transform.position = transform.position + (beamEndPosition - transform.position) / 2;
		//boxCollider.transform.LookAt(transform.position);
		currentColliderSize = boxCollider.size;
		boxCollider.size = new Vector2(currentColliderSize.x, (transform.position - beamEndPosition).magnitude);
		
		invokedBeam.GetComponent<BulletInfo>().SetDamage(damage);
        invokedBeam.GetComponent<BulletInfo>().SetIncrease(dmgIncreaseMultiplier);
		
		if(invokedParticles != null)
			Destroy(invokedParticles);
		Invoke("StopThaLaser", fireTime);
		Invoke("TwitchColliderForDamage", damageRate);
	}
	
	void StopThaLaser()
	{
		CancelInvoke("TwitchColliderForDamage");
		if(invokedBeam != null)
			Destroy(invokedBeam);
		if(chargeParticles != null && invokedParticles == null)
			invokedParticles = Instantiate(chargeParticles, transform.position, transform.rotation) as GameObject;
        Invoke("FireThaLaser", fireDelay);
	}
	
	void TwitchColliderForDamage()
	{
		boxCollider.enabled = false;
		boxCollider.enabled = true;
		Invoke("TwitchColliderForDamage", damageRate);
	}
	
	void OnDisable()
	{
		isEnabled = false;
		CancelInvoke();
		if(invokedBeam != null)
			Destroy(invokedBeam);
		if(invokedParticles != null)
			Destroy(invokedParticles);
	}
	
	void OnEnable()
	{
		isEnabled = true;
		if(chargeParticles != null && invokedParticles == null)
			invokedParticles = Instantiate(chargeParticles, transform.position, transform.rotation) as GameObject;
		CancelInvoke();
		Invoke("FireThaLaser", fireDelay);
	}
}
