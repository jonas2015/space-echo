﻿using UnityEngine;
using System.Collections;

public class RotateItself : MonoBehaviour
{
	public float speed = 1.0f;
	private int direction;

	// Use this for initialization
	void Start ()
	{
		direction = Random.Range(0, 2);
		if(direction == 0)
			direction = -1;
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.Rotate(0, 0, direction * speed);
	}
}
