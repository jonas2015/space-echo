﻿using UnityEngine;
using System.Collections;

public class SpinInAngle : MonoBehaviour
{
	public float angle = 1.0f;
	public float period = 1.0f;
    private float time;

	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
        time = time + Time.deltaTime;
        float phase = Mathf.Sin(time / period);
        transform.localRotation = Quaternion.Euler(new Vector3(0, 0, phase * angle));
	}
}
