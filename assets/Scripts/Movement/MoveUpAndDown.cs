﻿using UnityEngine;
using System.Collections;

public class MoveUpAndDown : MonoBehaviour
{
	public float rotationDelay = 1f;
	public float maxAngle = 1.0f;
	private float startingRotation;

	// Use this for initialization
	void Start ()
	{
		startingRotation = transform.eulerAngles.z;
		Invoke("Rotate", rotationDelay);
	}
	
	// Update is called once per frame
	void Rotate ()
	{
		Vector3 euler = transform.eulerAngles;
		euler.z = Random.Range(startingRotation - maxAngle, startingRotation + maxAngle);
		transform.eulerAngles = euler;
		Invoke("Rotate", rotationDelay);
	}
}
