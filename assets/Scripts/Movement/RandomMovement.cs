﻿using UnityEngine;
using System.Collections;

public class RandomMovement : MonoBehaviour
{
	public float moveAngle = 1.0f;
	public float rotationDelay = 1.0f;

	// Use this for initialization
	void Start ()
	{
		ChangeTrajectory();
	}
	
	// Update is called once per frame
	void Update ()
	{

	}

	void ChangeTrajectory()
	{
		float changeAngle = Random.Range(-1*moveAngle, moveAngle);
		transform.Rotate(0f, 0f, changeAngle);
		Invoke("ChangeTrajectory", rotationDelay);
	}
}
