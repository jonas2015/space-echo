﻿using UnityEngine;
using System.Collections;

public class MoveToPlayer : MonoBehaviour
{
	public float speed = 1.0f;
    private Vector3 playerPos = Vector3.zero;
	private Vector3 direction;
	private float distance;

	// Use this for initialization
	void Start ()
	{
		if(GameObject.FindGameObjectWithTag("Player") == null)
			Destroy(gameObject);
		else
		{
			playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;
			distance = Vector3.Distance(playerPos, transform.position);
			direction = (playerPos - transform.position) / distance;
		}
    }
    
    // Update is called once per frame
	void Update ()
	{
		if(playerPos != Vector3.zero)
			transform.position += direction * speed * Time.deltaTime;
		else if(playerPos == Vector3.zero)
			Destroy(gameObject);
	}
}
