﻿using UnityEngine;
using System.Collections;

public class MoveWithAcceleration : MonoBehaviour
{
	public float startingSpeed = 1.0f;
	public float acceleration = 1.0f;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		startingSpeed += acceleration * Time.deltaTime;
		transform.position += transform.up * startingSpeed * Time.deltaTime;
	}
}
