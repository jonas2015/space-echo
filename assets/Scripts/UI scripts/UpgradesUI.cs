﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UpgradesUI : MonoBehaviour
{
	public PlayerMovement playerMovement = null;
	public PlayerManager playerData = null;
	public GameObject mainPanel = null;
	public GameObject weaponRankPanel = null;
	public GameObject otherRankPanel = null;
	public GameObject frontGunPanel = null;
	public GameObject hullPanel = null;
	public GameObject shieldPanel = null;
	public GameObject generatorPanel = null;
	public GameObject leftGunPanel = null;
	public GameObject rightGunPanel = null;
    public GameObject statsPanel = null;
    public Text offensiveStatText = null;
    public Text defensiveStatText = null;
    public Text offensiveStatCostText = null;
    public Text defensiveStatCostText = null;
    public Text errorMessage = null;
	public Text moneyText = null;
	public Text minusText = null;
	public Text plusText = null;
	public GameObject[] GeneratorShadows = null;
	public GameObject[] GeneratorBars = null;
	public GameObject[] RankBars = null;
	public GameObject[] HullRankBars = null;
	public GameObject[] ShieldRankBars = null;
	private int maxRank = 5;
	private int[] hullCost = {0, 250, 1000};
	private int[] shieldCost = {0, 150, 500, 1000};
	private int[] generatorCost = {0, 100, 500, 1000};
	private int[] laserGunCost = {0, 0, 50, 200, 500, 900};
	private int[] matterCannonCost = {0, 25, 75, 150, 300, 500};
	private int[] bombLauncherCost = {0, 50, 100, 250, 500, 750};
	private int[] lightningCoilCost = {0, 50, 100, 150, 300, 500};
	private int[] multiCannonCost = {0, 50, 100, 150, 200, 300};
	private int[] pulseCannonCost = {0, 150, 300, 500, 750, 1000};
    private float offensiveStatsCost = 1000;
    private float defensiveStatsCost = 1000;
	private int[] shieldEnergy = {0, 1, 2, 3};
	private int[] laserGunEnergy = {0, 1, 1, 2, 3, 4};
	private int[] matterCannonEnergy = {0, 1, 2, 3, 4, 5};
	private int[] bombLauncherEnergy = {0, 1, 2, 3, 4, 5};
	private int[] lightningCoilEnergy = {0, 1, 2, 3, 4, 5};
	private int[] multiCannonEnergy = {0, 1, 1, 2, 2, 3};
	private int[] pulseCannonEnergy = {0, 1, 1, 2, 2, 3};
	private int[] generatorEnergy = {6, 12, 20, 25};
	private int currentEnergy = 0;
	private int maxEnergy = 0;
	private string activePanel = "main"; //"main", "frontGun", "hull", "shield", "generator", "leftGun", "rightGun", "stats"
	private string selectedGun = null;

	// Use this for initialization
	void Start ()
	{
		GameData.Load();
		UpdateMoneyText();
		UpdateCostText();
		UpdateShieldRankBars(GameData.shield);
		UpdateHullRankBars(GameData.hull);
		UpdateGeneratorBars(generatorEnergy[GameData.generator], "shadow");
		UpdateGeneratorBars(generatorEnergy[GameData.generator], "bars");
        offensiveStatsCost = Mathf.RoundToInt(offensiveStatsCost * Mathf.Pow(1.1f, GameData.offensiveStat));
        defensiveStatsCost = Mathf.RoundToInt(defensiveStatsCost * Mathf.Pow(1.1f, GameData.defensiveStat));
        UpdateStatsInfo();
		playerMovement.enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Input.GetKey(KeyCode.Escape))
			Back();
	}

	public void Back()
	{
		if(activePanel == "main")
		{
			GameData.Save();
			Application.LoadLevel("LevelSelectScene");
		}
		else
		{
			frontGunPanel.SetActive(false);
			otherRankPanel.SetActive(true);
			hullPanel.SetActive(false);
			shieldPanel.SetActive(false);
			generatorPanel.SetActive(false);
			leftGunPanel.SetActive(false);
			rightGunPanel.SetActive(false);
            statsPanel.SetActive(false);
			ShowPanel("main");
		}
	}

	public void ShowPanel(string name)
	{
		mainPanel.SetActive(false);
		activePanel = name;
		if(name == "main")
		{
			mainPanel.SetActive(true);
			weaponRankPanel.SetActive(false);
		}
		if(name == "frontGun")
		{
			if(selectedGun != null)
			{
				weaponRankPanel.SetActive(true);
				otherRankPanel.SetActive(false);
            }
            frontGunPanel.SetActive(true);
		}
		if(name == "hull")
		{
			hullPanel.SetActive(true);
		}
		if(name == "shield")
		{
			shieldPanel.SetActive(true);
		}
		if(name == "generator")
		{
			generatorPanel.SetActive(true);
		}
		if(name == "leftGun")
		{
			leftGunPanel.SetActive(true);
		}
		if(name == "rightGun")
		{
			rightGunPanel.SetActive(true);
		}
        if(name == "stats")
        {
            statsPanel.SetActive(true);
        }
	}
    //name:
	//"laserGun", "matterCannon", "bombLauncher", "lightningCoil", "multiCannonLeft", "multiCannonRight", "pulseCannonLeft", "pulseCannonRight"
	public void SelectGun(string name)
	{
		selectedGun = name;
		UpdateCostText();
		weaponRankPanel.SetActive(true);
		otherRankPanel.SetActive(false);
		if(name == "laserGun")
			UpdateRankBars(GameData.laserGun);
		if(name == "matterCannon")
			UpdateRankBars(GameData.matterCannon);
		if(name == "bombLauncher")
			UpdateRankBars(GameData.bombLauncher);
		if(name == "lightningCoil")
			UpdateRankBars(GameData.lightningCoil);
		if(name == "multiCannonLeft")
			UpdateRankBars(GameData.multiCannonLeft);
		if(name == "multiCannonRight")
			UpdateRankBars(GameData.multiCannonRight);
		if(name == "pulseCannonLeft")
			UpdateRankBars(GameData.pulseCannonLeft);
		if(name == "pulseCannonRight")
			UpdateRankBars(GameData.pulseCannonRight);
	}

	//name:
	//"hull1", "hull2", "hull3", "shield0", "shield1", "shield2", "shield3", "generator0", "generator1", "generator2", "generator3", "offensiveStat", "defensiveStat"
	public void SelectOther(string name) 
	{
		selectedGun = name;
		otherRankPanel.SetActive(true);
		weaponRankPanel.SetActive(false);
    }
    
    public void IncreaseRank()
	{
        if(selectedGun == "laserGun" && GameData.laserGun + 1 <= maxRank)
		{
            if(laserGunCost[GameData.laserGun + 1] <= GameData.money && EnoughEnergy("laserGun", GameData.laserGun + 1))
            {
                GameData.laserGun += 1;
                GameData.money -= laserGunCost[GameData.laserGun];
                UpdateRankBars(GameData.laserGun);
            }
            else if(laserGunCost[GameData.laserGun + 1] > GameData.money && !EnoughEnergy("laserGun", GameData.laserGun + 1))
                NotEnoughMoneyEnergy();
            else if(laserGunCost[GameData.laserGun + 1] > GameData.money && EnoughEnergy("laserGun", GameData.laserGun + 1))
                NotEnoughMoney();
            else if(laserGunCost[GameData.laserGun + 1] <= GameData.money && !EnoughEnergy("laserGun", GameData.laserGun + 1))
                NotEnoughEnergy();
		}
        else if(selectedGun == "laserGun" && GameData.laserGun + 1 > maxRank)
            MaxRankReached();

		if(selectedGun == "matterCannon" && GameData.matterCannon + 1 <= maxRank)
		{
			if(matterCannonCost[GameData.matterCannon+1] <= GameData.money && EnoughEnergy("matterCannon", GameData.matterCannon+1))
			{
				GameData.matterCannon += 1;
				GameData.money -= matterCannonCost[GameData.matterCannon];
				UpdateRankBars(GameData.matterCannon);
			}
            else if(matterCannonCost[GameData.matterCannon + 1] > GameData.money && !EnoughEnergy("matterCannon", GameData.matterCannon + 1))
                NotEnoughMoneyEnergy();
            else if(matterCannonCost[GameData.matterCannon + 1] > GameData.money && EnoughEnergy("matterCannon", GameData.matterCannon + 1))
                NotEnoughMoney();
            else if(matterCannonCost[GameData.matterCannon + 1] <= GameData.money && !EnoughEnergy("matterCannon", GameData.matterCannon + 1))
                NotEnoughEnergy();
		}
        else if(selectedGun == "matterCannon" && GameData.matterCannon + 1 > maxRank)
            MaxRankReached();

		if(selectedGun == "bombLauncher" && GameData.bombLauncher + 1 <= maxRank)
		{
			if(bombLauncherCost[GameData.bombLauncher+1] <= GameData.money && EnoughEnergy("bombLauncher", GameData.bombLauncher+1))
			{
				GameData.bombLauncher += 1;
				GameData.money -= bombLauncherCost[GameData.bombLauncher];
				UpdateRankBars(GameData.bombLauncher);
			}
            else if(bombLauncherCost[GameData.bombLauncher + 1] > GameData.money && !EnoughEnergy("bombLauncher", GameData.bombLauncher + 1))
                NotEnoughMoneyEnergy();
            else if(bombLauncherCost[GameData.bombLauncher + 1] > GameData.money && EnoughEnergy("bombLauncher", GameData.bombLauncher + 1))
                NotEnoughMoney();
            else if(bombLauncherCost[GameData.bombLauncher + 1] <= GameData.money && !EnoughEnergy("bombLauncher", GameData.bombLauncher + 1))
                NotEnoughEnergy();
		}
        else if(selectedGun == "bombLauncher" && GameData.bombLauncher + 1 > maxRank)
            MaxRankReached();

		if(selectedGun == "lightningCoil" && GameData.lightningCoil + 1 <= maxRank)
		{
			if(lightningCoilCost[GameData.lightningCoil+1] <= GameData.money && EnoughEnergy("lightningCoil", GameData.lightningCoil+1))
			{
				GameData.lightningCoil += 1;
				GameData.money -= lightningCoilCost[GameData.lightningCoil];
				UpdateRankBars(GameData.lightningCoil);
			}
            else if(lightningCoilCost[GameData.lightningCoil + 1] > GameData.money && !EnoughEnergy("lightningCoil", GameData.lightningCoil + 1))
                NotEnoughMoneyEnergy();
            else if(lightningCoilCost[GameData.lightningCoil + 1] > GameData.money && EnoughEnergy("lightningCoil", GameData.lightningCoil + 1))
                NotEnoughMoney();
            else if(lightningCoilCost[GameData.lightningCoil + 1] <= GameData.money && !EnoughEnergy("lightningCoil", GameData.lightningCoil + 1))
                NotEnoughEnergy();
		}
        else if(selectedGun == "lightningCoil" && GameData.lightningCoil + 1 > maxRank)
            MaxRankReached();

		if(selectedGun == "multiCannonLeft" && GameData.multiCannonLeft + 1 <= maxRank)
		{
			if(multiCannonCost[GameData.multiCannonLeft+1] <= GameData.money && EnoughEnergy("multiCannonLeft", GameData.multiCannonLeft+1))
			{
				GameData.multiCannonLeft += 1;
				GameData.money -= multiCannonCost[GameData.multiCannonLeft];
				UpdateRankBars(GameData.multiCannonLeft);
			}
            else if(multiCannonCost[GameData.multiCannonLeft + 1] > GameData.money && !EnoughEnergy("multiCannonLeft", GameData.multiCannonLeft + 1))
                NotEnoughMoneyEnergy();
            else if(multiCannonCost[GameData.multiCannonLeft + 1] > GameData.money && EnoughEnergy("multiCannonLeft", GameData.multiCannonLeft + 1))
                NotEnoughMoney();
            else if(multiCannonCost[GameData.multiCannonLeft + 1] <= GameData.money && !EnoughEnergy("multiCannonLeft", GameData.multiCannonLeft + 1))
                NotEnoughEnergy();
		}
        else if(selectedGun == "multiCannonLeft" && GameData.multiCannonLeft + 1 > maxRank)
            MaxRankReached();

		if(selectedGun == "multiCannonRight" && GameData.multiCannonRight + 1 <= maxRank)
		{
			if(multiCannonCost[GameData.multiCannonRight+1] <= GameData.money && EnoughEnergy("multiCannonRight", GameData.multiCannonRight+1))
			{
				GameData.multiCannonRight += 1;
				GameData.money -= multiCannonCost[GameData.multiCannonRight];
				UpdateRankBars(GameData.multiCannonRight);
			}
            else if(multiCannonCost[GameData.multiCannonRight + 1] > GameData.money && !EnoughEnergy("multiCannonRight", GameData.multiCannonRight + 1))
                NotEnoughMoneyEnergy();
            else if(multiCannonCost[GameData.multiCannonRight + 1] > GameData.money && EnoughEnergy("multiCannonRight", GameData.multiCannonRight + 1))
                NotEnoughMoney();
            else if(multiCannonCost[GameData.multiCannonRight + 1] <= GameData.money && !EnoughEnergy("multiCannonRight", GameData.multiCannonRight + 1))
                NotEnoughEnergy();
		}
        else if(selectedGun == "multiCannonRight" && GameData.multiCannonRight + 1 > maxRank)
            MaxRankReached();

		if(selectedGun == "pulseCannonLeft" && GameData.pulseCannonLeft + 1 <= maxRank)
		{
			if(pulseCannonCost[GameData.pulseCannonLeft+1] <= GameData.money && EnoughEnergy("pulseCannonLeft", GameData.pulseCannonLeft+1))
			{
				GameData.pulseCannonLeft += 1;
				GameData.money -= pulseCannonCost[GameData.pulseCannonLeft];
				UpdateRankBars(GameData.pulseCannonLeft);
			}
            else if(pulseCannonCost[GameData.pulseCannonLeft + 1] > GameData.money && !EnoughEnergy("pulseCannonLeft", GameData.pulseCannonLeft + 1))
                NotEnoughMoneyEnergy();
            else if(pulseCannonCost[GameData.pulseCannonLeft + 1] > GameData.money && EnoughEnergy("pulseCannonLeft", GameData.pulseCannonLeft + 1))
                NotEnoughMoney();
            else if(pulseCannonCost[GameData.pulseCannonLeft + 1] <= GameData.money && !EnoughEnergy("pulseCannonLeft", GameData.pulseCannonLeft + 1))
                NotEnoughEnergy();
		}
        else if(selectedGun == "pulseCannonLeft" && GameData.pulseCannonLeft + 1 > maxRank)
            MaxRankReached();

		if(selectedGun == "pulseCannonRight" && GameData.pulseCannonRight + 1 <= maxRank)
		{
			if(pulseCannonCost[GameData.pulseCannonRight+1] <= GameData.money && EnoughEnergy("pulseCannonRight", GameData.pulseCannonRight+1))
			{
				GameData.pulseCannonRight += 1;
				GameData.money -= pulseCannonCost[GameData.pulseCannonRight];
				UpdateRankBars(GameData.pulseCannonRight);
			}
            else if(pulseCannonCost[GameData.pulseCannonRight + 1] > GameData.money && !EnoughEnergy("pulseCannonRight", GameData.pulseCannonRight + 1))
                NotEnoughMoneyEnergy();
            else if(pulseCannonCost[GameData.pulseCannonRight + 1] > GameData.money && EnoughEnergy("pulseCannonRight", GameData.pulseCannonRight + 1))
                NotEnoughMoney();
            else if(pulseCannonCost[GameData.pulseCannonRight + 1] <= GameData.money && !EnoughEnergy("pulseCannonRight", GameData.pulseCannonRight + 1))
                NotEnoughEnergy();
		}
        else if(selectedGun == "pulseCannonRight" && GameData.pulseCannonRight + 1 > maxRank)
            MaxRankReached();

		UpdateMoneyText();
		playerData.UpdateData();
		UpdateCostText();
		UpdateGeneratorBars(GameData.generator, "bars");
	}
			
	public void DecreaseRank()
	{
        if(selectedGun == "laserGun" && GameData.laserGun - 1 >= 0)
        {
            GameData.money += laserGunCost[GameData.laserGun];
            GameData.laserGun -= 1;
            UpdateRankBars(GameData.laserGun);
        }
        else if(selectedGun == "laserGun" && GameData.laserGun - 1 < 0)
            MinRankReached();

		if(selectedGun == "matterCannon" && GameData.matterCannon - 1 >= 0)
		{
			GameData.money += matterCannonCost[GameData.matterCannon];
			GameData.matterCannon -= 1;
			UpdateRankBars(GameData.matterCannon);
		}
        else if(selectedGun == "matterCannon" && GameData.matterCannon - 1 < 0)
            MinRankReached();

		if(selectedGun == "bombLauncher" && GameData.bombLauncher - 1 >= 0)
		{
			GameData.money += bombLauncherCost[GameData.bombLauncher];
			GameData.bombLauncher -= 1;
			UpdateRankBars(GameData.bombLauncher);
		}
        else if(selectedGun == "bombLauncher" && GameData.bombLauncher - 1 < 0)
            MinRankReached();

		if(selectedGun == "lightningCoil" && GameData.lightningCoil - 1 >= 0)
		{
			GameData.money += lightningCoilCost[GameData.lightningCoil];
			GameData.lightningCoil -= 1;
			UpdateRankBars(GameData.lightningCoil);
		}
        else if(selectedGun == "lightningCoil" && GameData.lightningCoil - 1 < 0)
            MinRankReached();

		if(selectedGun == "multiCannonLeft" && GameData.multiCannonLeft - 1 >= 0)
		{
			GameData.money += multiCannonCost[GameData.multiCannonLeft];
			GameData.multiCannonLeft -= 1;
			UpdateRankBars(GameData.multiCannonLeft);
		}
        else if(selectedGun == "multiCannonLeft" && GameData.multiCannonLeft - 1 < 0)
            MinRankReached();

		if(selectedGun == "multiCannonRight" && GameData.multiCannonRight - 1 >= 0)
		{
			GameData.money += multiCannonCost[GameData.multiCannonRight];
			GameData.multiCannonRight -= 1;
			UpdateRankBars(GameData.multiCannonRight);
		}
        else if(selectedGun == "multiCannonRight" && GameData.multiCannonRight - 1 < 0)
            MinRankReached();

		if(selectedGun == "pulseCannonLeft" && GameData.pulseCannonLeft - 1 >= 0)
		{
			GameData.money += pulseCannonCost[GameData.pulseCannonLeft];
			GameData.pulseCannonLeft -= 1;
			UpdateRankBars(GameData.pulseCannonLeft);
		}
        else if(selectedGun == "pulseCannonLeft" && GameData.pulseCannonLeft - 1 < 0)
            MinRankReached();

		if(selectedGun == "pulseCannonRight" && GameData.pulseCannonRight - 1 >= 0)
		{
			GameData.money += pulseCannonCost[GameData.pulseCannonRight];
			GameData.pulseCannonRight -= 1;
			UpdateRankBars(GameData.pulseCannonRight);
		}
        else if(selectedGun == "pulseCannonRight" && GameData.pulseCannonRight - 1 < 0)
            MinRankReached();

		playerData.UpdateData();
		UpdateMoneyText();
		UpdateCostText();
		UpdateGeneratorBars(GameData.generator, "bars");
	}

	public void BuyOther()
	{
        if(selectedGun == "hull1" && GameData.money >= hullCost[0])
        {
            GameData.money += hullCost[GameData.hull];
            GameData.money -= hullCost[0];
            GameData.hull = 0;
        }
        else if(selectedGun == "hull1" && GameData.money < hullCost[0])
            NotEnoughMoney();

		if(selectedGun == "hull2" && GameData.money >= hullCost[1])
		{
			GameData.money += hullCost[GameData.hull];
			GameData.money -= hullCost[1];
            GameData.hull = 1;
        }
        else if(selectedGun == "hull2" && GameData.money < hullCost[1])
            NotEnoughMoney();

		if(selectedGun == "hull3" && GameData.money >= hullCost[2])
		{
			GameData.money += hullCost[GameData.hull];
			GameData.money -= hullCost[2];
			GameData.hull = 2;
        }
        else if(selectedGun == "hull3" && GameData.money < hullCost[2])
            NotEnoughMoney();

		if(selectedGun == "shield0")
		{
			GameData.money += shieldCost[GameData.shield];
            GameData.money -= shieldCost[0];
            GameData.shield = 0;
        }
        else if(selectedGun == "shield0" && GameData.money < shieldCost[0])
            NotEnoughMoney();

        if(selectedGun == "shield1" && GameData.money >= shieldCost[1] && EnoughEnergy("shield", 1))
        {
            GameData.money += shieldCost[GameData.shield];
            GameData.money -= shieldCost[1];
            GameData.shield = 1;
        }
        else if(selectedGun == "shield1" && GameData.money < shieldCost[1] && !EnoughEnergy("shield", 1))
            NotEnoughMoneyEnergy();
        else if(selectedGun == "shield1" && GameData.money < shieldCost[1] && EnoughEnergy("shield", 1))
            NotEnoughMoney();
        else if(selectedGun == "shield1" && GameData.money >= shieldCost[1] && !EnoughEnergy("shield", 1))
            NotEnoughEnergy();

		if(selectedGun == "shield2" && GameData.money >= shieldCost[2] && EnoughEnergy("shield", 2))
		{
			GameData.money += shieldCost[GameData.shield];
			GameData.money -= shieldCost[2];
			GameData.shield = 2;
		}
        else if(selectedGun == "shield2" && GameData.money < shieldCost[2] && !EnoughEnergy("shield", 2))
            NotEnoughMoneyEnergy();
        else if(selectedGun == "shield2" && GameData.money < shieldCost[2] && EnoughEnergy("shield", 2))
            NotEnoughMoney();
        else if(selectedGun == "shield2" && GameData.money >= shieldCost[2] && !EnoughEnergy("shield", 2))
            NotEnoughEnergy();

		if(selectedGun == "shield3" && GameData.money >= shieldCost[3] && EnoughEnergy("shield", 3))
		{
			GameData.money += shieldCost[GameData.shield];
			GameData.money -= shieldCost[3];
			GameData.shield = 3;
        }
        else if(selectedGun == "shield3" &&  GameData.money < shieldCost[3] && !EnoughEnergy("shield", 3))
            NotEnoughMoneyEnergy();
        else if(selectedGun == "shield3" &&  GameData.money < shieldCost[3] && EnoughEnergy("shield", 3))
            NotEnoughMoney();
        else if(selectedGun == "shield3" &&  GameData.money >= shieldCost[3] && !EnoughEnergy("shield", 3))
            NotEnoughEnergy();

		if(selectedGun == "generator0" && generatorEnergy[0] >= currentEnergy)
		{
			GameData.money += generatorCost[GameData.generator];
			GameData.generator = 0;
		}
        else if(selectedGun == "generator0" && generatorEnergy[0] < currentEnergy)
            EnergyBeingUsed();

        if(selectedGun == "generator1" && GameData.money >= generatorCost[1] && generatorEnergy[1] >= currentEnergy)
        {
            GameData.money += generatorCost[GameData.generator];
            GameData.money -= generatorCost[1];
            GameData.generator = 1;
        }
        else if(selectedGun == "generator1" && GameData.money >= generatorCost[1] && generatorEnergy[1] < currentEnergy)
            EnergyBeingUsed();
        else if(selectedGun == "generator1" && GameData.money < generatorCost[1] && generatorEnergy[1] >= currentEnergy)
            NotEnoughMoney();

        if(selectedGun == "generator2" && GameData.money >= generatorCost[2] && generatorEnergy[2] >= currentEnergy)
		{
			GameData.money += generatorCost[GameData.generator];
			GameData.money -= generatorCost[2];
			GameData.generator = 2;
		}
        else if(selectedGun == "generator2" && GameData.money >= generatorCost[2] && generatorEnergy[2] < currentEnergy)
            EnergyBeingUsed();
        else if(selectedGun == "generator2" && GameData.money < generatorCost[2] && generatorEnergy[2] >= currentEnergy)
            NotEnoughMoney();

        if(selectedGun == "generator3" && GameData.money >= generatorCost[3] && generatorEnergy[3] >= currentEnergy)
		{
			GameData.money += generatorCost[GameData.generator];
			GameData.money -= generatorCost[3];
			GameData.generator = 3;
        }
        else if(selectedGun == "generator3" && GameData.money >= generatorCost[3] && generatorEnergy[3] < currentEnergy)
            EnergyBeingUsed();
        else if(selectedGun == "generator3" && GameData.money < generatorCost[3] && generatorEnergy[3] >= currentEnergy)
            NotEnoughMoney();

        if(selectedGun == "offensiveStat" && GameData.money >= offensiveStatsCost)
        {
            GameData.money -= offensiveStatsCost;
            GameData.offensiveStat += 1;
            offensiveStatsCost = Mathf.RoundToInt(1000 * Mathf.Pow(1.1f, GameData.offensiveStat));
        }
        else if(selectedGun == "offensiveStat" && GameData.money < offensiveStatsCost)
            NotEnoughMoney();

        if(selectedGun == "defensiveStat" && GameData.money >= defensiveStatsCost)
        {
            GameData.money -= defensiveStatsCost;
            GameData.defensiveStat += 1;
            defensiveStatsCost = Mathf.RoundToInt(1000 * Mathf.Pow(1.1f, GameData.defensiveStat));
        }
        else if(selectedGun == "defensiveStat" && GameData.money < defensiveStatsCost)
            NotEnoughMoney();

        UpdateMoneyText();
		playerData.UpdateData();
		UpdateHullRankBars(GameData.hull);
		UpdateShieldRankBars(GameData.shield);
		UpdateGeneratorBars(generatorEnergy[GameData.generator], "shadow");
		UpdateGeneratorBars(generatorEnergy[GameData.generator], "bars");
        UpdateStatsInfo();
	}

	void UpdateMoneyText()
	{
		moneyText.text = ((int) GameData.money).ToString();
	}

	void UpdateCostText()
	{
		if(selectedGun == null)
		{
			minusText.text = "";
			plusText.text = "";
		}
		if(selectedGun == "laserGun")
		{
			if(GameData.laserGun-1 < 0)
				minusText.text = "";
			else
				minusText.text = laserGunCost[GameData.laserGun].ToString();
			if(GameData.laserGun+1 > maxRank)
				plusText.text = "";
			else
				plusText.text = laserGunCost[GameData.laserGun+1].ToString();
		}
		if(selectedGun == "matterCannon")
		{
			if(GameData.matterCannon-1 < 0)
				minusText.text = "";
			else
				minusText.text = matterCannonCost[GameData.matterCannon].ToString();
			if(GameData.matterCannon+1 > maxRank)
				plusText.text = "";
			else
				plusText.text = matterCannonCost[GameData.matterCannon+1].ToString();
		}
		if(selectedGun == "bombLauncher")
		{
			if(GameData.bombLauncher-1 < 0)
				minusText.text = "";
			else
				minusText.text = bombLauncherCost[GameData.bombLauncher].ToString();
			if(GameData.bombLauncher+1 > maxRank)
				plusText.text = "";
			else
				plusText.text = bombLauncherCost[GameData.bombLauncher+1].ToString();
		}
		if(selectedGun == "lightningCoil")
		{
			if(GameData.lightningCoil-1 < 0)
				minusText.text = "";
			else
				minusText.text = lightningCoilCost[GameData.lightningCoil].ToString();
			if(GameData.lightningCoil+1 > maxRank)
				plusText.text = "";
			else
				plusText.text = lightningCoilCost[GameData.lightningCoil+1].ToString();
		}
		if(selectedGun == "multiCannonLeft")
		{
			if(GameData.multiCannonLeft-1 < 0)
				minusText.text = "";
			else
				minusText.text = multiCannonCost[GameData.multiCannonLeft].ToString();
			if(GameData.multiCannonLeft+1 > maxRank)
				plusText.text = "";
			else
				plusText.text = multiCannonCost[GameData.multiCannonLeft+1].ToString();
		}
		if(selectedGun == "multiCannonRight")
		{
			if(GameData.multiCannonRight-1 < 0)
				minusText.text = "";
			else
				minusText.text = multiCannonCost[GameData.multiCannonRight].ToString();
			if(GameData.multiCannonRight+1 > maxRank)
				plusText.text = "";
			else
				plusText.text = multiCannonCost[GameData.multiCannonRight+1].ToString();
		}
		if(selectedGun == "pulseCannonLeft")
		{
			if(GameData.pulseCannonLeft-1 < 0)
				minusText.text = "";
			else
				minusText.text = pulseCannonCost[GameData.pulseCannonLeft].ToString();
			if(GameData.pulseCannonLeft+1 > maxRank)
				plusText.text = "";
			else
				plusText.text = pulseCannonCost[GameData.pulseCannonLeft+1].ToString();
		}
		if(selectedGun == "pulseCannonRight")
		{
			if(GameData.pulseCannonRight-1 < 0)
				minusText.text = "";
			else
				minusText.text = pulseCannonCost[GameData.pulseCannonRight].ToString();
			if(GameData.pulseCannonRight+1 > maxRank)
				plusText.text = "";
			else
				plusText.text = pulseCannonCost[GameData.pulseCannonRight+1].ToString();
		}
	}

	bool EnoughEnergy(string type, int nRank)
	{
		if(type == "shield")
		{
			if(shieldEnergy[nRank] - shieldEnergy[GameData.shield] <= maxEnergy - currentEnergy)
				return true;
		}
		if(type == "laserGun")
		{
			if(laserGunEnergy[nRank] - laserGunEnergy[GameData.laserGun] <= maxEnergy - currentEnergy)
				return true;
		}
		if(type == "matterCannon")
		{
			if(matterCannonEnergy[nRank] - matterCannonEnergy[GameData.matterCannon] <= maxEnergy - currentEnergy)
				return true;
		}
		if(type == "bombLauncher")
		{
			if(bombLauncherEnergy[nRank] - bombLauncherEnergy[GameData.bombLauncher] <= maxEnergy - currentEnergy)
				return true;
		}
		if(type == "lightningCoil")
		{
			if(lightningCoilEnergy[nRank] - lightningCoilEnergy[GameData.lightningCoil] <= maxEnergy - currentEnergy)
				return true;
		}
		if(type == "multiCannonLeft")
		{
			if(multiCannonEnergy[nRank] - multiCannonEnergy[GameData.multiCannonLeft] <= maxEnergy - currentEnergy)
				return true;
		}
		if(type == "multiCannonRight")
		{
			if(multiCannonEnergy[nRank] - multiCannonEnergy[GameData.multiCannonRight] <= maxEnergy - currentEnergy)
				return true;
		}
		if(type == "pulseCannonLeft")
		{
			if(pulseCannonEnergy[nRank] - pulseCannonEnergy[GameData.pulseCannonLeft] <= maxEnergy - currentEnergy)
				return true;
		}
		if(type == "pulseCannonRight")
		{
			if(pulseCannonEnergy[nRank] - pulseCannonEnergy[GameData.pulseCannonRight] <= maxEnergy - currentEnergy)
				return true;
		}
		return false;
	}

	void UpdateRankBars(int num)
	{
		for(int i = 0; i < 5; i++)
		{
			RankBars[i].SetActive(false);
        }

		for(int i = 0; i < num; i++)
		{
			RankBars[i].SetActive(true);
		}
	}

	void UpdateHullRankBars(int num)
	{
		for(int i = 0; i < 3; i++)
		{
			HullRankBars[i].SetActive(false);
		}
		
		for(int i = 0; i < num + 1; i++)
		{
			HullRankBars[i].SetActive(true);
        }
    }

	void UpdateShieldRankBars(int num)
	{
		for(int i = 0; i < 3; i++)
		{
			ShieldRankBars[i].SetActive(false);
		}
		
		for(int i = 0; i < num; i++)
		{
			ShieldRankBars[i].SetActive(true);
        }
    }

    void UpdateStatsInfo()
    {
        offensiveStatText.text = GameData.offensiveStat.ToString();
        defensiveStatText.text = GameData.defensiveStat.ToString();
        offensiveStatCostText.text = Mathf.RoundToInt(offensiveStatsCost).ToString();
        defensiveStatCostText.text = Mathf.RoundToInt(defensiveStatsCost).ToString();
    }

	void UpdateGeneratorBars(int num, string type)
	{
		if(type == "shadow")
		{
			maxEnergy = generatorEnergy[GameData.generator];
			for(int i = 0; i < 25; i++)
			{
				GeneratorShadows[i].SetActive(false);
            }
			for(int i = 0; i < num; i++)
			{
				GeneratorShadows[i].SetActive(true);
	        }
		}
		if(type == "bars")
		{
			currentEnergy = shieldEnergy[GameData.shield] + laserGunEnergy[GameData.laserGun] + matterCannonEnergy[GameData.matterCannon] + bombLauncherEnergy[GameData.bombLauncher]
							+ lightningCoilEnergy[GameData.lightningCoil] + multiCannonEnergy[GameData.multiCannonLeft] + multiCannonEnergy[GameData.multiCannonRight]
							+ pulseCannonEnergy[GameData.pulseCannonLeft] + pulseCannonEnergy[GameData.pulseCannonRight];
			for(int i = 0; i < 25; i++)
			{
				GeneratorBars[i].SetActive(false);
            }
			for(int i = 0; i < currentEnergy; i++)
			{
				GeneratorBars[i].SetActive(true);
	        }
		}
    }

    void NotEnoughMoney()
    {
        errorMessage.enabled = true;
        CancelInvoke("DisableErrorText");
        errorMessage.CrossFadeAlpha(100f, 0.5f, false);
        errorMessage.text = "Not enough money!";
        errorMessage.CrossFadeAlpha(0f, 2.0f, false);
        Invoke("DisableErrorText", 2f);
    }

    void NotEnoughEnergy()
    {
        errorMessage.enabled = true;
        CancelInvoke("DisableErrorText");
        errorMessage.CrossFadeAlpha(100f, 0.5f, false);
        errorMessage.text = "Not enough energy!";
        errorMessage.CrossFadeAlpha(0f, 2.0f, false);
        Invoke("DisableErrorText", 2f);
    }

    void NotEnoughMoneyEnergy()
    {
        errorMessage.enabled = true;
        CancelInvoke("DisableErrorText");
        errorMessage.CrossFadeAlpha(100f, 0.5f, false);
        errorMessage.text = "Not enough money and energy!";
        errorMessage.CrossFadeAlpha(0f, 2.0f, false);
        Invoke("DisableErrorText", 2f);
    }

    void MinRankReached()
    {
        errorMessage.enabled = true;
        CancelInvoke("DisableErrorText");
        errorMessage.CrossFadeAlpha(100f, 0.5f, false);
        errorMessage.text = "Minimum rank rached!";
        errorMessage.CrossFadeAlpha(0f, 2.0f, false);
        Invoke("DisableErrorText", 2f);
    }

    void MaxRankReached()
    {
        errorMessage.enabled = true;
        CancelInvoke("DisableErrorText");
        errorMessage.CrossFadeAlpha(100f, 0.5f, false);
        errorMessage.text = "Maximum rank reached!";
        errorMessage.CrossFadeAlpha(0f, 2.0f, false);
        Invoke("DisableErrorText", 2f);
    }

    void EnergyBeingUsed()
    {
        errorMessage.enabled = true;
        CancelInvoke("DisableErrorText");
        errorMessage.CrossFadeAlpha(100f, 0.5f, false);
        errorMessage.text = "Energy is being used!";
        errorMessage.CrossFadeAlpha(0f, 2.0f, false);
        Invoke("DisableErrorText", 2f);
    }

    void DisableErrorText()
    {
        errorMessage.enabled = false;
    }
}