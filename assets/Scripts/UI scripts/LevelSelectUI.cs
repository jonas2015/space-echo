﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelSelectUI : MonoBehaviour
{
	public string selectedLevel = "Level1";
	public GameObject selectedGraphic;
	public Text selectedLevelNameText;
	public Text waveNumber;
	private int selectedWave = 1;

	// Use this for initialization
	void Start ()
	{
		GameData.Load();
		FixZeroWaves ();
		ChangeLevelName(selectedLevel);
		selectedWave = GameData.level1Wave;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Input.GetKey(KeyCode.Escape))
			Back();
	}

	public void Back()
	{
		Application.LoadLevel("MainMenu");
	}

	public void Upgrades()
	{
		Application.LoadLevel("UpgradesScene");
	}

	public void StartGame()
	{
		Application.LoadLevel(selectedLevel);
	}

	public void SelectLevelPlanet(GameObject planet, string levelName)
	{
		selectedGraphic.transform.position = planet.transform.position;
		selectedLevel = levelName;
		ChangeLevelName(selectedLevel);
	}

	public void IncreaseWave()
	{
		if(selectedLevel == "Level1" && selectedWave < GameData.level1Wave)
			selectedWave++;
		else if(selectedLevel == "Level2" && selectedWave < GameData.level2Wave)
			selectedWave++;
		else if(selectedLevel == "Level3" && selectedWave < GameData.level3Wave)
			selectedWave++;
		changeWaveNumber(selectedWave);
	}

	public void DecreaseWave()
	{
		if(selectedWave > 1)
			selectedWave--;
		changeWaveNumber(selectedWave);
	}

	void changeWaveNumber(int num)
	{
		waveNumber.text = num.ToString();
		GameData.currentWave = num;
	}

	void ChangeLevelName(string level)
	{
		if(level == "Level1")
		{
			selectedLevelNameText.text = "Level 1";
			changeWaveNumber(GameData.level1Wave);
			selectedWave = GameData.level1Wave;
		}
		if(level == "Level2")
		{
			selectedLevelNameText.text = "Level 2";
			changeWaveNumber(GameData.level2Wave);
			selectedWave = GameData.level2Wave;
		}
		if(level == "Level3")
		{
			selectedLevelNameText.text = "Level 3";
			changeWaveNumber(GameData.level3Wave);
			selectedWave = GameData.level3Wave;
		}

		GameData.currentLevel = level;
		GameData.currentWave = selectedWave;
	}

	void FixZeroWaves()
	{
		if (GameData.level1Wave == 0)
			GameData.level1Wave = 1;
		if (GameData.level2Wave == 0)
			GameData.level2Wave = 1;
		if (GameData.level3Wave == 0)
			GameData.level3Wave = 1;

		GameData.Save ();
	}
}
