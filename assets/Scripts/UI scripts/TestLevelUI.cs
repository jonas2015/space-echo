﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Audio;

public class TestLevelUI : MonoBehaviour
{
	public Text moneyText = null;
	public Text moneyGottenText = null;
	public PlayerManager playerData = null;
	public GameObject pauseMenu = null;
	public GameObject deadMenu = null;
	public float money;
    public AudioMixer masterMixer;
	private float startingMoney = 0.0f;
	private bool soundOn = true;
	private bool musicOn = true;
	private bool isMenuActive = false;
	private bool isDeadMenuActive = false;
	private bool isPlayerAlive = true;
	private GameObject playerGO;

	// Use this for initialization
	void Start ()
	{
		GameData.Load();
		playerData.UpdateData();
		money = GameData.money;
        startingMoney = GameData.money;
        InvokeRepeating("GiveMoney", 1, 1);
	}
	
	// Update is called once per frame
	void Update ()
	{
		playerGO = GameObject.FindGameObjectWithTag("Player");
		if(playerGO == null)
		{
			isPlayerAlive = false;
			if(isDeadMenuActive == false)
				Invoke("ShowDeadMenu", 2.0f);
		}

		moneyText.text = ((int)money).ToString();
	}

    void GiveMoney()
    {
        if(isPlayerAlive)
            money += 1 + ((int) GameData.currentWave / 5);
    }

	public void ShowMenu()
	{
		if(pauseMenu != null && isMenuActive)
		{
			Time.timeScale = 1;
			pauseMenu.SetActive(false);
			isMenuActive = false;
		}
		else if(pauseMenu != null && playerGO != null)
		{
			Time.timeScale = 0;
			pauseMenu.SetActive(true);
			isMenuActive = true;
		}
		GameData.money = money;
		GameData.Save();
	}

	void ShowDeadMenu()
	{
		isDeadMenuActive = true;
		deadMenu.SetActive(true);
		moneyGottenText.text = ((int)money - (int)startingMoney).ToString();
		GameData.money = money;
		GameData.Save();
	}

	public void SoundChange()
	{
		if(soundOn == true)
		{
			soundOn = false;
            masterMixer.SetFloat("FXVol", -100f);
		}
		else if(soundOn == false)
		{
			soundOn = true;
            masterMixer.SetFloat("FXVol", GameData.soundVolume);
		}
	}

	public void MusicChange()
	{
		if(musicOn == true)
		{
			musicOn = false;
            masterMixer.SetFloat("MusicVol", -100f);
		}
		else if(musicOn == false)
		{
			musicOn = true;
            masterMixer.SetFloat("MusicVol", GameData.musicVolume);
		}
	}

	public void Resume()
	{
		if(isMenuActive == true && pauseMenu != null)
		{
			Time.timeScale = 1;
			pauseMenu.SetActive(false);
			isMenuActive = false;
		}
	}

	public void Quit()
	{
		Time.timeScale = 1;
		GameData.money = money;
        masterMixer.SetFloat("FXVol", GameData.soundVolume);
        masterMixer.SetFloat("MusicVol", GameData.musicVolume);
		GameData.Save();
		Application.LoadLevel("LevelSelectScene");
	}
}
