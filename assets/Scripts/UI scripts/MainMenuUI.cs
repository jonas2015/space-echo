﻿using UnityEngine;
using System.Collections;

public class MainMenuUI : MonoBehaviour
{
	public GameObject quitDialog = null;
	private bool isDialogActive = false;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		//Debug.Log(isDialogActive);
		if(Input.GetKey(KeyCode.Escape) && isDialogActive == false)
			ShowQuitDialog();
		if(Input.GetKey(KeyCode.Escape) && isDialogActive == true)
			QuitGame();
	}

	public void StartGame()
	{
		Application.LoadLevel("LevelSelectScene");
	}

	public void OptionsScreen()
	{
		Application.LoadLevel("OptionsScene");
	}

    public void CreditsScreen()
    {
        Application.LoadLevel("CreditsScene");
    }

	public void QuitGame()
	{
		Application.Quit();
	}

	public void ShowQuitDialog()
	{
		quitDialog.SetActive(true);
		isDialogActive = true;
	}

	public void HideQuitDialog()
	{
		quitDialog.SetActive(false);
		isDialogActive = false;
	}
}
