﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Audio;

public class OptionsUI : MonoBehaviour
{
	public GameObject rlyPanel = null;
    public AudioMixer masterMixer;
    public Slider musicSlider = null;
    public Slider soundSlider = null;
    public AudioSource soundDemo = null;
    public Toggle lowButton = null;
    public Toggle medButton = null;
    public Toggle highButton = null;
	private bool isRlyActive = false;

	// Use this for initialization
	void Start ()
	{
        GameData.Load();
        ChangeToggle();
        musicSlider.value = GameData.musicVolume;
        soundSlider.value = GameData.soundVolume;
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void Back()
	{
		if(isRlyActive == true)
		{
			rlyPanel.SetActive(false);
			isRlyActive = false;
		}
        else if(isRlyActive == false)
        {
            GameData.Save();
            Application.LoadLevel("MainMenu");
        }
	}

	public void ShowRlyPanel()
	{
		rlyPanel.SetActive(true);
		isRlyActive = true;
	}

    public void SetMusicVol(float vol)
    {
        masterMixer.SetFloat("MusicVol", vol);
        GameData.musicVolume = vol;
    }

    public void SetSoundVol(float vol)
    {
        masterMixer.SetFloat("FXVol", vol);
        if(soundDemo != null)
            soundDemo.Play();
        GameData.soundVolume = vol;
    }

    public void SetDefault()
    {
        musicSlider.value = 0;
        soundSlider.value = 0;
    }

    public void ChangeQuality(int num)
    {
        if(num == 1)
        {
            QualitySettings.currentLevel = QualityLevel.Fastest;
            GameData.qualityLevel = 1;
        }
        if(num == 2)
        {
            QualitySettings.currentLevel = QualityLevel.Simple;
            GameData.qualityLevel = 2;
        }
        if(num == 3)
        {
            QualitySettings.currentLevel = QualityLevel.Fantastic;
            GameData.qualityLevel = 3;
        }
    }

	public void ResetProgress()
	{
		GameData.ResetProgress();
		if(isRlyActive == true)
		{
			rlyPanel.SetActive(false);
			isRlyActive = false;
		}
	}

	public void GiveMoney()
	{
		GameData.money = 9999999;
		GameData.Save();
	}

    void ChangeToggle()
    {
        if(GameData.qualityLevel == 1)
            lowButton.isOn = true;
        if(GameData.qualityLevel == 2)
            medButton.isOn = true;
        if(GameData.qualityLevel == 3)
            highButton.isOn = true;
    }
}
