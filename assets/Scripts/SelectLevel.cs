﻿using UnityEngine;
using System.Collections;

public class SelectLevel : MonoBehaviour
{
	public LevelSelectUI levelScript = null;
	public string levelName = "";

	// Use this for initialization
	void Start ()
	{
		
	}

	void OnMouseDown()
	{
		levelScript.SelectLevelPlanet(gameObject, levelName);
	}
}
