﻿using UnityEngine;
using System.Collections;

public class RemoveTag : MonoBehaviour
{
	public bool isTagOn = true;
	private string currentTag;

	// Use this for initialization
	void Start ()
	{
		currentTag = gameObject.tag;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(isTagOn)
			gameObject.tag = "Enemy";
		else
			gameObject.tag = "Untagged";
	}
}
