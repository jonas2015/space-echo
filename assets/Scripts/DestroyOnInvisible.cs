﻿using UnityEngine;
using System.Collections;

public class DestroyOnInvisible : MonoBehaviour
{
	public GameObject destroyTarget = null;
	public bool isDelay = true;

	void OnBecameInvisible()
	{
		if(isDelay)
			Invoke("DestroyObject", 1.0f);
		else
			DestroyObject();
	}

	void DestroyObject()
	{
		if(destroyTarget == null)
			Destroy(gameObject);
		else
			Destroy(destroyTarget);
	}
}
