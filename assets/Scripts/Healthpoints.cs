﻿using UnityEngine;
using System.Collections;

public class Healthpoints : MonoBehaviour
{
	public float initialHealth = 10;
	public float increaseMultiplier = 0f;
	public GameObject hitEffect = null;
	public GameObject deathEffect = null;
	public float currentHealth;
	private bool isDead = false;
	
	// Use this for initialization
	void Start () 
	{
        if(increaseMultiplier != 0)
            initialHealth = initialHealth * Mathf.Pow(increaseMultiplier, GameData.currentWave);
		currentHealth = initialHealth;
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if(isDead || col.tag == "Player")
			return;

		float dmg = col.GetComponent<BulletInfo>().damage;
		currentHealth -= dmg;

        if(col.tag == "PlayerBullets" && col.GetComponent<DestroyOnDemand>() != null)
            col.GetComponent<DestroyOnDemand>().shouldDestroy = true;

		if(hitEffect)
			Instantiate(hitEffect, col.transform.position, Quaternion.identity);
		
		if(currentHealth <= 0)
		{
			if(GetComponent<SpawnMeteors>() != null)
				GetComponent<SpawnMeteors>().SpawnMeteorsOnDestroy();
			isDead = true;
			
			if(deathEffect)
				Instantiate(deathEffect, transform.position, Quaternion.identity);
            if(GetComponent<AwardMoney>() != null)
                GetComponent<AwardMoney>().Award();
			Destroy(gameObject);
		}
	}
}
