﻿using UnityEngine;
using System.Collections;

public class Level1BossBehaviour : MonoBehaviour
{
	public GameObject boss1;
	public GameObject boss2;
	public GameObject boss3;
	private Animator boss2Animator;
	private Animator boss3Animator;
	private int activeBoss = 1;
	private bool boss2Charging = false;
	private bool boss3Charging = false;

	// Use this for initialization
	void Start ()
	{
		boss2Animator = boss2.GetComponent<Animator>();
		boss3Animator = boss3.GetComponent<Animator>();
		Invoke("RandomEvent", 5f);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(boss1 == null && activeBoss == 1 && !boss2Charging)
			Boss2Active();
		if(boss1 == null && boss2 == null && activeBoss == 2 && !boss3Charging)
			Boss3Active();
		if(boss1 == null && boss2 == null && boss3 == null)
			Destroy(gameObject);
	}

	public void DisableBoss1()
	{
		boss1.transform.position = new Vector3(0f, -100f, 0f);
		boss1.SetActive(false);
	}

	public void DisableBoss2()
	{
		boss2Charging = false;
		boss2.transform.position = new Vector3(0f, -100f, 0f);
		boss2.SetActive(false);
	}

	public void DisableBoss3()
	{
		boss3Charging = false;
		boss3.transform.position = new Vector3(0f, -100f, 0f);
		boss3.SetActive(false);
	}

	public void EnableBoss1()
	{
		boss1.SetActive(true);
	}
	
	public void EnableBoss2()
	{
		boss2.SetActive(true);
	}
	
	public void EnableBoss3()
	{
		boss3.SetActive(true);
	}

	private void RandomEvent()
	{
		if(activeBoss != 2 && boss2 != null)
			DisableBoss2();
		if(activeBoss != 3 && boss3 != null)
			DisableBoss3();
		if(activeBoss == 1)
		{
			int whichBoss = Random.Range(1, 3);
			if(whichBoss == 1)
				Boss2Charge();
			if(whichBoss == 2)
				Boss3Charge();
		}
		if(activeBoss == 2)
		{
			Boss3Charge();
		}
	}

	private void Boss2Charge()
	{
		boss2Charging = true;
		EnableBoss2();
		int whichPlace = Random.Range(1, 4);
		boss2Animator.SetInteger("ChargePosition", whichPlace);
		Invoke("ResetIntegers", 0.5f);
		Invoke("DisableBoss2", 2.5f);
		Invoke("RandomEvent", 10f);
	}

	private void Boss3Charge()
	{
		boss3Charging = true;
		EnableBoss3();
		int whichPlace = Random.Range(1, 3);
		boss3Animator.SetInteger("ChargePosition", whichPlace);
		Invoke("ResetIntegers", 0.5f);
		Invoke("DisableBoss3", 5f);
		Invoke("RandomEvent", 10f);
	}

	private void Boss2Active()
	{
		activeBoss = 2;
		EnableBoss2();
		boss2Animator.SetInteger("LinearMovement", 1);
		Invoke("ResetIntegers", 0.5f);
	}

	private void Boss3Active()
	{
		activeBoss = 3;
		EnableBoss3();
		boss3Animator.SetInteger("Chilling", 1);
		Invoke("ResetIntegers", 0.5f);
	}

	private void ResetIntegers()
	{
		if(boss2 != null)
		{
			boss2Animator.SetInteger("ChargePosition", 0);
			boss2Animator.SetInteger("LinearMovement", 0);
		}
		if(boss3 != null)
		{
			boss3Animator.SetInteger("ChargePosition", 0);
			boss3Animator.SetInteger("Chilling", 0);
		}
	}
}
