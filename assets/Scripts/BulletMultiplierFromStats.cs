﻿using UnityEngine;
using System.Collections;

public class BulletMultiplierFromStats : MonoBehaviour
{
	// Use this for initialization
	void Start ()
    {
        BulletInfo bltInfo = GetComponent<BulletInfo>();
        bltInfo.SetDamage(bltInfo.damage * Mathf.Pow(1.1f, GameData.offensiveStat));
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
