﻿using UnityEngine;
using System.Collections;

public class DestroyOnDemand : MonoBehaviour
{
    public bool shouldDestroy = false;
    public GameObject destroyEffect = null;

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(shouldDestroy)
            DestroyThis();
	}

    void DestroyThis()
    {
        if(destroyEffect != null)
        {
            Instantiate(destroyEffect, transform.position, destroyEffect.transform.rotation);
			if(destroyEffect.GetComponent<BulletInfo>() != null)
				destroyEffect.GetComponent<BulletInfo>().damage = GetComponent<BulletInfo>().damage * 3;
        }
        Destroy(gameObject);
    }
}
