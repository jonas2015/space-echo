﻿using UnityEngine;
using System.Collections;

public class SpawnMeteors : MonoBehaviour
{
	public GameObject[] meteorToSpawn = null;
	public int amountToSpawn = 1;
	public float spawnAngleRadius = 1.0f;
	private float spawnAngle;

	// Use this for initialization
	void Start ()
	{
		if(amountToSpawn > 1)
			spawnAngle = -1 * (spawnAngleRadius / 2);
	}

	public void SpawnMeteorsOnDestroy()
	{
		for(int i = 0; i < amountToSpawn; i++)
		{
			int whichToSpawn = Random.Range(0, meteorToSpawn.Length);
			GameObject go = Instantiate(meteorToSpawn[whichToSpawn], transform.position, transform.rotation) as GameObject;
			go.transform.Rotate(0, spawnAngle, 0);
			go.name = go.name.Replace("(Clone)", "");
			spawnAngle += spawnAngleRadius / (amountToSpawn -1);
		}
	}
}
