﻿using UnityEngine;
using System.Collections;

public class DestroyOnTrigger : MonoBehaviour
{
	public GameObject destroyEffect = null;

	// Use this for initialization
	void Start()
	{
	
	}

	void OnTriggerEnter2D()
	{
		if(destroyEffect !=null)
		{
			Instantiate(destroyEffect, transform.position, destroyEffect.transform.rotation);
			if(destroyEffect.GetComponent<BulletInfo>() != null)
				destroyEffect.GetComponent<BulletInfo>().damage = GetComponent<BulletInfo>().damage * 3;
		}
		Destroy(gameObject);
	}
}
