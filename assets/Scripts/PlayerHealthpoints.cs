﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHealthpoints : MonoBehaviour
{
	public GameObject hitEffect = null;
	public GameObject deathEffect = null;
	public Image healthbar = null;
	public Image shieldbar = null;
	public Image healthGrey = null;
	public Image shieldGrey = null;
	public float rechargeDelay = 3f;
	public GameObject[] shieldImages = null;
	private float rechargeSpeed = 10f;
	private bool isRecharging = false;
	private int[] healthpoints = { 100, 150, 200 };
	private int[] shieldpoints = { 0, 100, 150, 200 };
    private int healthRank;
    private int shieldRank;
    private float maxHealth;
    private float maxShield;

    [SerializeField]
	private float currentHealth;
    [SerializeField]
	private float currentShield;
	

	// Use this for initialization
	void Start ()
	{
		GameData.Load();
		UpdateData();
		currentHealth = healthpoints[healthRank];
		currentShield = shieldpoints[shieldRank];
        UpdateWithStats();

        if(healthpoints[2] >= currentHealth)
            maxHealth = healthpoints[2];
        else if(healthpoints[2] < currentHealth)
            maxHealth = currentHealth;

        if(shieldpoints[3] >= currentShield)
            maxShield = shieldpoints[3];
        else if(shieldpoints[3] < currentShield)
            maxShield = currentShield;

		if(healthGrey != null && shieldGrey != null)
		{
            healthGrey.fillAmount = currentHealth * 100 / (float) maxHealth / 100;
            shieldGrey.fillAmount = currentShield * 100 / (float) maxShield / 100;
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(healthbar != null && shieldbar != null)
		{
			if(isRecharging && currentShield < shieldpoints[shieldRank])
				currentShield += rechargeSpeed * Time.deltaTime;
            healthbar.fillAmount = currentHealth * 100 / (float) maxHealth / 100;
            shieldbar.fillAmount = currentShield * 100 / (float) maxShield / 100;
		}
	}

	void OnTriggerEnter2D(Collider2D col)
	{
        float dmg = 0;
		isRecharging = false;
		CancelInvoke();
        if(col.GetComponent<BulletInfo>() != null)
		    dmg = col.GetComponent<BulletInfo>().damage;

        if(col.tag == "EnemyBullets" && col.GetComponent<DestroyOnDemand>() != null)
            col.GetComponent<DestroyOnDemand>().shouldDestroy = true;

		if(currentShield <= 0)
		{
			currentHealth -= dmg;
			if(shieldRank != 0 && shieldImages[shieldRank-1] != null)
				shieldImages[shieldRank-1].SetActive(false);
		}
		else if(currentShield > 0)
			currentShield -= dmg;

		Invoke("StartRecharging", rechargeDelay);

		if(hitEffect)
			Instantiate(hitEffect, col.transform.position, Quaternion.identity);
		
		if(currentHealth <= 0)
		{			
			currentHealth = 0;
			healthbar.fillAmount = 0;
			if(deathEffect)
				Instantiate(deathEffect, col.transform.position, Quaternion.identity);
			Destroy(gameObject);
		}
	}

	void StartRecharging()
	{
		isRecharging = true;
        if(currentShield < 0)
            currentShield = 0;
		if(shieldRank != 0 && shieldImages[shieldRank-1] != null)
			shieldImages[shieldRank-1].SetActive(true);
	}

    void UpdateWithStats()
    {
        currentHealth = currentHealth * Mathf.Pow(1.1f, GameData.defensiveStat);
        currentShield = currentShield * Mathf.Pow(1.1f, GameData.defensiveStat);
    }

	public void UpdateData()
	{
		healthRank = GameData.hull;
		shieldRank = GameData.shield;
	}
}
