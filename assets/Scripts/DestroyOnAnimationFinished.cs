﻿using UnityEngine;
using System.Collections;

public class DestroyOnAnimationFinished : MonoBehaviour
{
	public GameObject destroyTarget = null;
	public bool shouldDestroy = false;

	void Update()
	{
		if(shouldDestroy)
			DestroyThis();
	}

	void DestroyThis()
	{
		if(destroyTarget == null)
			Destroy(gameObject);
		else
			Destroy(destroyTarget);
	}
}
