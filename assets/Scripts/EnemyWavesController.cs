﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class EnemyWavesController : MonoBehaviour
{
	public GameObject enemy1;
    public GameObject enemy2;
    public GameObject enemy3;
    public GameObject enemy4;
	public GameObject enemy5;
	public GameObject enemy6;
	public GameObject boss;
	public GameObject[] bigMeteorBrown;
	public GameObject[] bigMeteorGrey;
	public GameObject[] mediumMeteorBrown;
	public GameObject[] mediumMeteorGrey;
	public GameObject[] smallMeteorBrown;
	public GameObject[] smallMeteorGrey;
	public GameObject spawnLeft;
	public GameObject spawnMiddle;
	public GameObject spawnRight;
	public GameObject spawnZero;
	public GameObject newWaveText = null;
    public Text currentWaveText = null;

	private string currentLevel = "";
    [SerializeField]
	private int currentWave = 1;
	private int enemyCount = 0;
	private GameObject playerGO;

	// Use this for initialization
	void Start ()
	{
		currentLevel = GameData.currentLevel;
		currentWave = GameData.currentWave;
		playerGO = GameObject.FindGameObjectWithTag("Player");
        currentWaveText.text = currentWave.ToString();
		StartCoroutine(StartWave());
	}
	
	// Update is called once per frame
	void Update ()
	{
		playerGO = GameObject.FindGameObjectWithTag("Player");
		GameObject[] enemies;
		enemies = GameObject.FindGameObjectsWithTag("Enemy");
		enemyCount = enemies.Length;
	}

	IEnumerator StartWave()
	{
		while(true && playerGO != null)
		{
            currentWaveText.text = currentWave.ToString();
			int amountToSpawn = 5 + currentWave;

			yield return new WaitForSeconds(1.0f);
			newWaveText.transform.GetChild(0).GetComponent<Text>().text = "WAVE " + currentWave + " STARTING!";
			newWaveText.SetActive(true);
			yield return new WaitForSeconds(0.5f);
			newWaveText.transform.GetChild(0).GetComponent<Text>().CrossFadeAlpha(0f, 2.0f, false);
			yield return new WaitForSeconds(2.0f);
			if(currentWave%5 == 0)
			{
				newWaveText.SetActive(false);
				newWaveText.SetActive(true);
				newWaveText.transform.GetChild(0).GetComponent<Text>().text = "BOSS WAVE!";
				yield return new WaitForSeconds(0.5f);
				newWaveText.transform.GetChild(0).GetComponent<Text>().CrossFadeAlpha(0f, 2.0f, false);
				yield return new WaitForSeconds(2.0f);
			}
			newWaveText.SetActive(false);

			if(currentWave%5 != 0)
			{
                for(int j = 0; j < 1 + (int) Mathf.Sqrt(currentWave); j++)
                {
                    int enemyType = Random.Range(1, 7);
                    Debug.Log("Enemy type: " + enemyType);

                    while(enemyCount > 0)
                    {
                        GameObject plzwork = GameObject.FindGameObjectWithTag("Enemy");
                        Debug.Log(enemyCount + ", " + plzwork.tag);
                        yield return new WaitForSeconds(0.1f);
                    }

                    for(int i = 0; i < amountToSpawn; i++)
                    {
                        if(enemyType == 1)
                        {
                            int placeToSpawn = Random.Range(0, 3);

                            if(placeToSpawn == 0)
                                SpawnEnemies(enemy1, spawnLeft);
                            if(placeToSpawn == 1)
                                SpawnEnemies(enemy1, spawnMiddle);
                            if(placeToSpawn == 2)
                                SpawnEnemies(enemy1, spawnRight);
                            yield return new WaitForSeconds(1.0f);
                        }
                        if(enemyType == 2)
                        {
                            SpawnEnemy2();
                            yield return new WaitForSeconds(0.5f);
                        }
                        if(enemyType == 4)
                        {
                            SpawnEnemies(enemy4, spawnZero);
                            yield return new WaitForSeconds(0.5f);
                        }
						if(enemyType == 5)
						{
							SpawnEnemies(enemy5, spawnZero);
							yield return new WaitForSeconds(0.5f);
						}
						if(enemyType == 6)
						{
							SpawnEnemy6();
							yield return new WaitForSeconds(0.5f);
						}
                    }

                    if(enemyType == 3)
                    {
                        for(int i = 0; i < 8; i++)
                        {
                            SpawnEnemy3(i);
                            yield return new WaitForSeconds(0.5f);
                        }
                    }
                }
			}
			else if(currentWave%5 == 0)
			{
                SpawnBoss(boss);
				yield return new WaitForSeconds(1f);
			}

			while(enemyCount > 0)
			{
				yield return new WaitForSeconds(0.1f);
			}

			if(enemyCount <= 0)
			{
				currentWave++;
				UpdateWaveData(currentWave);
			}
		}
	}

	void SpawnEnemies(GameObject objectToSpawn, GameObject placeToSpawn)
	{
		GameObject go = Instantiate(objectToSpawn, placeToSpawn.transform.position, placeToSpawn.transform.rotation) as GameObject;
		go.name = go.name.Replace("(Clone)", "");
	}

    void SpawnEnemy2()
    {
        GameObject go = Instantiate(enemy2, Vector3.zero, Quaternion.identity) as GameObject;
        go.name = go.name.Replace("(Clone)", "");
        go.GetComponent<Animator>().Play("Enemy2ExitRight");

        go = Instantiate(enemy2, Vector3.zero, Quaternion.identity) as GameObject;
        go.name = go.name.Replace("(Clone)", "");
        go.GetComponent<Animator>().Play("Enemy2ExitLeft");
    }

    void SpawnEnemy3(int num)
    {
        Vector3 _pos = new Vector3(-7 + (2 * num), 0f, 0f);
        GameObject empty = new GameObject("enemy3Parent");

        GameObject go = Instantiate(enemy3, Vector3.zero, Quaternion.identity) as GameObject;
        go.name = go.name.Replace("(Clone)", "");
        go.transform.parent = empty.transform;
        empty.transform.position = _pos;
    }

	void SpawnEnemy6()
	{
		int _dir = Random.Range (1, 3);
		GameObject go = Instantiate(enemy6, Vector3.zero, Quaternion.identity) as GameObject;
		go.name = go.name.Replace("(Clone)", "");
		if(_dir == 1)
			go.GetComponent<Animator>().Play("Enemy6EntryLeft");
		else if(_dir == 2)
			go.GetComponent<Animator>().Play("Enemy6EntryRight");
	}

	void SpawnBoss(GameObject objectToSpawn)
    {
        GameObject go = Instantiate(objectToSpawn, Vector3.zero, Quaternion.identity) as GameObject;
        go.name = go.name.Replace("(Clone)", "");
    }

	void UpdateWaveData(int num)
	{
        GameData.currentWave = currentWave;
		if(currentLevel == "Level1" && num > GameData.level1Wave)
			GameData.level1Wave = num;
		else if(currentLevel == "Level2" && num > GameData.level2Wave)
			GameData.level2Wave = num;
		else if(currentLevel == "Level3" && num > GameData.level3Wave)
			GameData.level3Wave = num;

		GameData.Save();
	}
}
