﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public float speed = 1.0f;
	private float lMovement = 0f;
	private float rMovement = 0f;
	private string direction = "";

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		//#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_BLACKBERRY && !UNITY_WINRT
		transform.position += new Vector3(Input.GetAxis("Horizontal"), 0.0f, 0.0f) * speed * Time.deltaTime;
		//#endif

		if(direction == "left" && lMovement != 0f)
			transform.position += new Vector3(lMovement, 0.0f, 0.0f) * speed * Time.deltaTime;
		else if(direction == "left" && lMovement == 0f && rMovement != 0f)
			transform.position += new Vector3(rMovement, 0.0f, 0.0f) * speed * Time.deltaTime;
		else if(direction == "right" && rMovement != 0f)
			transform.position += new Vector3(rMovement, 0.0f, 0.0f) * speed * Time.deltaTime;
		else if(direction == "right" && rMovement == 0f && lMovement != 0f)
			transform.position += new Vector3(lMovement, 0.0f, 0.0f) * speed * Time.deltaTime;
		else
			transform.position += new Vector3(0.0f, 0.0f, 0.0f) * speed * Time.deltaTime;

		Vector3 viewPos = Camera.main.WorldToViewportPoint(transform.position);
		viewPos.x = Mathf.Clamp01(viewPos.x);
		transform.position = Camera.main.ViewportToWorldPoint(viewPos);
	}

	public void LeftMovement(float dir)
	{
		lMovement = dir;
		if(dir != 0f)
			direction = "left";
	}

	public void RightMovement(float dir)
	{
		rMovement = dir;
		if(dir != 0f)
			direction = "right";
	}
}
