﻿using UnityEngine;
using System.Collections;

public class SpawnObject : MonoBehaviour
{
	public GameObject objectToSpawn;
	public float spawnDelay = 1.0f;
	
	// Use this for initialization
	void Start () 
	{
		Invoke("Spawn", spawnDelay);
	}
	
	void Spawn () 
	{
		GameObject go = Instantiate(objectToSpawn, transform.position, transform.rotation) as GameObject;
		go.name = go.name.Replace("(Clone)", "");
		Invoke("Spawn", spawnDelay);
	}
}
