﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class Shoot : MonoBehaviour
{
	public GameObject bullet;
    public GameObject muzzleEffect = null;
	public float delay = 0.2f;
	public float damage = 1f;
	public float dmgIncreaseMultiplier = 0f;
	private bool readyToShoot = true;
	
	// Update is called once per frame
	void Update () 
	{
		if(readyToShoot)
		{
            if(muzzleEffect != null)
                Instantiate(muzzleEffect, transform.position, transform.rotation);
			GameObject go = Instantiate(bullet, transform.position, transform.rotation) as GameObject;
			go.name = go.name.Replace("(Clone)", "");
			go.GetComponent<BulletInfo>().SetDamage(damage);
            go.GetComponent<BulletInfo>().SetIncrease(dmgIncreaseMultiplier);
			readyToShoot = false;
			Invoke("ResetReadyToShoot", delay);
		}
	}
	
	void ResetReadyToShoot()
	{
		readyToShoot = true;
	}
}
