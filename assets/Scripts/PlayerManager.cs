﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour
{
	public GameObject[] laserBarrel;
	public GameObject[] matterCannonBarrel;
	public GameObject[] bombLauncherBarrel;
	public GameObject[] lightningCoilBarrel;
	public GameObject[] multiCannonLeftBarrel;
	public GameObject[] multiCannonRightBarrel;
	public GameObject[] pulseCannonLeftBarrel;
	public GameObject[] pulseCannonRightBarrel;
	public GameObject[] hullImages;
	public GameObject[] shieldImages;
	private int hull;
    private int shield;
    private int laserGun;
    private int matterCannon;
    private int bombLauncher;
    private int lightningCoil;
    private int multiCannonLeft;
    private int multiCannonRight;
    private int pulseCannonLeft;
    private int pulseCannonRight;
	
	// Use this for initialization
	void Start ()
	{
		UpdateData();
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void UpdateData()
	{
		//Debug.Log(laserGun);
		LoadData();

		//-------HULL------------------------------
		if(hullImages[hull] != null)
		{
			ResetActive(hullImages);
			hullImages[hull].SetActive(true);
        }

		//-------SHIELD------------------------------
		if(shield == 0 && shieldImages != null)
			ResetActive(shieldImages);
		else if(shieldImages[shield-1] != null)
		{
			ResetActive(shieldImages);
			shieldImages[shield-1].SetActive(true);
        }

		//-------LASER GUN------------------------------
		if(laserGun == 0 && laserBarrel != null)
			ResetActive(laserBarrel);
		else if(laserBarrel[laserGun-1] != null)
		{
			ResetActive(laserBarrel);
			laserBarrel[laserGun-1].SetActive(true);
		}
		
		//-------MATTER CANNON--------------------------
		if(matterCannon == 0 && matterCannonBarrel != null)
			ResetActive(matterCannonBarrel);
		else if(matterCannonBarrel[matterCannon-1] != null)
		{
			ResetActive(matterCannonBarrel);
			matterCannonBarrel[matterCannon-1].SetActive(true);
		}

		//-------BOMB LAUNCHER--------------------------
		if(bombLauncher == 0 && bombLauncherBarrel != null)
			ResetActive(bombLauncherBarrel);
		else if(bombLauncherBarrel[bombLauncher-1] != null)
		{
			ResetActive(bombLauncherBarrel);
			bombLauncherBarrel[bombLauncher-1].SetActive(true);
		}

		//-------LIGHTNING COIL--------------------------
		if(lightningCoil == 0 && lightningCoilBarrel != null)
			ResetActive(lightningCoilBarrel);
		else if(lightningCoilBarrel[lightningCoil-1] != null)
		{
			ResetActive(lightningCoilBarrel);
			lightningCoilBarrel[lightningCoil-1].SetActive(true);
		}

		//-------MULTI CANNON LEFT--------------------------
		if(multiCannonLeft == 0 && multiCannonLeftBarrel != null)
			ResetActive(multiCannonLeftBarrel);
		else if(multiCannonLeftBarrel[multiCannonLeft-1] != null)
		{
			ResetActive(multiCannonLeftBarrel);
			multiCannonLeftBarrel[multiCannonLeft-1].SetActive(true);
		}

		//-------MULTI CANNON RIGHT--------------------------
		if(multiCannonRight == 0 && multiCannonRightBarrel != null)
			ResetActive(multiCannonRightBarrel);
		else if(multiCannonRightBarrel[multiCannonRight-1] != null)
		{
			ResetActive(multiCannonRightBarrel);
			multiCannonRightBarrel[multiCannonRight-1].SetActive(true);
		}

		//-------PULSE CANNON LEFT--------------------------
		if(pulseCannonLeft == 0 && pulseCannonLeftBarrel != null)
			ResetActive(pulseCannonLeftBarrel);
		else if(pulseCannonLeftBarrel[pulseCannonLeft-1] != null)
		{
			ResetActive(pulseCannonLeftBarrel);
			pulseCannonLeftBarrel[pulseCannonLeft-1].SetActive(true);
		}
		
		//-------PULSE CANNON RIGHT--------------------------
		if(pulseCannonRight == 0 && pulseCannonRightBarrel != null)
			ResetActive(pulseCannonRightBarrel);
		else if(pulseCannonRightBarrel[pulseCannonRight-1] != null)
		{
			ResetActive(pulseCannonRightBarrel);
			pulseCannonRightBarrel[pulseCannonRight-1].SetActive(true);
		}
	}
	
	void ResetActive(GameObject[] _array)
	{
		foreach(GameObject _obj in _array)
			_obj.SetActive(false);
	}

	public void LoadData()
	{
		hull = GameData.hull;
		shield = GameData.shield;
		laserGun = GameData.laserGun;
		matterCannon = GameData.matterCannon;
		bombLauncher = GameData.bombLauncher;
		lightningCoil = GameData.lightningCoil;
		multiCannonLeft = GameData.multiCannonLeft;
		multiCannonRight = GameData.multiCannonRight;
		pulseCannonLeft = GameData.pulseCannonLeft;
		pulseCannonRight = GameData.pulseCannonRight;
	}
}
