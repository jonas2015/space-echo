﻿using UnityEngine;
using System.Collections;

public class BulletInfo : MonoBehaviour
{
	public float damage = 1.0f;
	public float increaseMultiplier = 0f;

	void Start()
	{
        if(increaseMultiplier != 0)
            damage = damage * Mathf.Pow(increaseMultiplier, GameData.currentWave);
	}

	public void SetDamage(float set)
	{
		damage = set;
	}

	public void SetIncrease(float set)
	{
        increaseMultiplier = set;
	}
}
