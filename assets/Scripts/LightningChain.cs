﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightningChain : MonoBehaviour
{
	public GameObject lightningChain = null;
	public int preferredTarget = 1;
	public float maxDistance = 1.0f;
	public float damageRate = 1.0f;
	public float damage = 1.0f;
	private GameObject spawnedLightning = null;
	private Vector2 currentColliderSize;
	private BoxCollider2D lightningCollider;
	private LineRenderer lightningLineRenderer;

	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
		List<GameObject> enemiesList = new List<GameObject>(enemies);
		Vector3 randomEndPos;

		//lightningCollider.center = Vector3.zero;
		//lightningCollider.transform.LookAt(transform.position);

		lightningLineRenderer.SetPosition(0, transform.position);

		if(enemiesList.Count >= preferredTarget)
		{
			enemiesList.Sort(SortByDistance);

			randomEndPos = new Vector3(enemiesList[preferredTarget-1].transform.position.x + Random.Range(-0.1f, 0.1f), 
			                           enemiesList[preferredTarget-1].transform.position.y + Random.Range(-0.1f, 0.1f),
			                           enemiesList[preferredTarget-1].transform.position.z + Random.Range(-0.1f, 0.1f));

			if(Vector3.Distance(randomEndPos, transform.position) <= maxDistance)
			{
				//Debug.Log(Vector3.Distance(randomEndPos, transform.position));

				lightningLineRenderer.SetPosition(1, randomEndPos);

				Color lightColor = new Color(0.5647f, 0.58823f, 1f, Random.Range (0.2f, 1f));
				lightningLineRenderer.SetColors(lightColor, lightColor);

				lightningCollider.transform.position = transform.position + (enemiesList[0].transform.position - transform.position) / 2;
				lightningCollider.size = new Vector2(currentColliderSize.x, (randomEndPos - transform.position).magnitude);
			}
			else
			{
				lightningLineRenderer.SetPosition(1, transform.position);
				lightningCollider.transform.position = transform.position;
				lightningCollider.size = Vector2.zero;
			}
		}
		else
		{
			lightningLineRenderer.SetPosition(1, transform.position);
			lightningCollider.transform.position = transform.position;
			lightningCollider.size = Vector2.zero;
		}
	}

	void TwitchColliderForDamage()
	{
		if(spawnedLightning != null)
		{
			lightningCollider.enabled = false;
			lightningCollider.enabled = true;
			Invoke("TwitchColliderForDamage", damageRate);
		}
	}

	int SortByDistance(GameObject a, GameObject b)
	{
		float distToA = Vector3.Distance(transform.position, a.transform.position);
		float distToB = Vector3.Distance(transform.position, b.transform.position);
		return distToA.CompareTo(distToB);
	}

	void OnDisable()
	{
		CancelInvoke("TwitchColliderForDamage");
		if(spawnedLightning != null)
			Destroy(spawnedLightning);
	}

	void OnEnable()
	{
		spawnedLightning = Instantiate(lightningChain, transform.position, transform.rotation) as GameObject;
		spawnedLightning.name = spawnedLightning.name.Replace("(Clone)", "");
		lightningCollider = spawnedLightning.GetComponent<BoxCollider2D>();
		lightningLineRenderer = spawnedLightning.GetComponent<LineRenderer>();
		currentColliderSize = lightningCollider.size;
		spawnedLightning.GetComponent<BulletInfo>().damage = damage;
		Invoke("TwitchColliderForDamage", damageRate);
	}
}
