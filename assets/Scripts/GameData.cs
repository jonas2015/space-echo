﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class GameData
{
	public static int laserGun = 1;
	public static int matterCannon = 0;
	public static int bombLauncher = 0;
	public static int lightningCoil = 0;
	public static int multiCannonLeft = 0;
	public static int multiCannonRight = 0;
	public static int pulseCannonLeft = 0;
	public static int pulseCannonRight = 0;
	public static int hull = 0;
	public static int shield = 0;
	public static int generator = 0;
	public static float money = 0.0f;
	public static int level1Wave = 1;
	public static int level2Wave = 1;
	public static int level3Wave = 1;
    public static int offensiveStat = 0;
    public static int defensiveStat = 0;
    public static float musicVolume = 0f;
    public static float soundVolume = 0f;
    public static int qualityLevel = 2;

	public static string currentLevel = "Level1";
	public static int currentWave = 1;

	public static void Save()
	{
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/gameSave.ktu");
		
		PlayerData data = new PlayerData();
		data.laserGun = laserGun;
		data.matterCannon = matterCannon;
		data.bombLauncher = bombLauncher;
		data.lightningCoil = lightningCoil;
		data.multiCannonLeft = multiCannonLeft;
		data.multiCannonRight = multiCannonRight;
		data.pulseCannonLeft = pulseCannonLeft;
		data.pulseCannonRight = pulseCannonRight;
		data.hull = hull;
		data.shield = shield;
		data.generator = generator;
		data.money = money;
		data.level1Wave = level1Wave;
		data.level2Wave = level2Wave;
		data.level3Wave = level3Wave;
        data.offensiveStat = offensiveStat;
        data.defensiveStat = defensiveStat;
        data.musicVolume = musicVolume;
        data.soundVolume = soundVolume;
        data.qualityLevel = qualityLevel;
		
		bf.Serialize(file, data);
		file.Close();
	}

	public static void Load()
	{
		if(File.Exists(Application.persistentDataPath + "/gameSave.ktu"))
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/gameSave.ktu", FileMode.Open);
			PlayerData data = (PlayerData)bf.Deserialize(file);
			file.Close();

			laserGun = data.laserGun;
			matterCannon = data.matterCannon;
			bombLauncher = data.bombLauncher;
			lightningCoil = data.lightningCoil;
			multiCannonLeft = data.multiCannonLeft;
			multiCannonRight = data.multiCannonRight;
			pulseCannonLeft = data.pulseCannonLeft;
			pulseCannonRight = data.pulseCannonRight;
			hull = data.hull;
			shield = data.shield;
			generator = data.generator;
			money = data.money;
			level1Wave = data.level1Wave;
			level2Wave = data.level2Wave;
			level3Wave = data.level3Wave;
            offensiveStat = data.offensiveStat;
            defensiveStat = data.defensiveStat;
            musicVolume = data.musicVolume;
            soundVolume = data.soundVolume;
            qualityLevel = data.qualityLevel;
		}
	}

	public static void ResetProgress()
	{
		if(File.Exists(Application.persistentDataPath + "/gameSave.ktu"))
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Create(Application.persistentDataPath + "/gameSave.ktu");
			PlayerData data = new PlayerData();
			bf.Serialize(file, data);
			file.Close();
			Load();
		}
		else
		{
			PlayerData data = new PlayerData();
			laserGun = data.laserGun;
			matterCannon = data.matterCannon;
			bombLauncher = data.bombLauncher;
			lightningCoil = data.lightningCoil;
			multiCannonLeft = data.multiCannonLeft;
			multiCannonRight = data.multiCannonRight;
			pulseCannonLeft = data.pulseCannonLeft;
			pulseCannonRight = data.pulseCannonRight;
			hull = data.hull;
			shield = data.shield;
			generator = data.generator;
			money = data.money;
			level1Wave = data.level1Wave;
			level2Wave = data.level2Wave;
			level3Wave = data.level3Wave;
            offensiveStat = data.offensiveStat;
            defensiveStat = data.defensiveStat;
            musicVolume = data.musicVolume;
            soundVolume = data.soundVolume;
            qualityLevel = data.qualityLevel;
		}
	}
}

[Serializable]
class PlayerData
{
	public int laserGun = 1;
	public int matterCannon = 0;
	public int bombLauncher = 0;
	public int lightningCoil = 0;
	public int multiCannonLeft = 0;
	public int multiCannonRight = 0;
	public int pulseCannonLeft = 0;
	public int pulseCannonRight = 0;
	public int hull = 0;
	public int shield = 0;
	public int generator = 0;
	public float money = 0;
	public int level1Wave = 1;
	public int level2Wave = 1;
	public int level3Wave = 1;
    public int offensiveStat = 0;
    public int defensiveStat = 0;
    public float musicVolume = 0f;
    public float soundVolume = 0f;
    public int qualityLevel = 2;
}
